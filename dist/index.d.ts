import * as React from 'react';
export declare const createConsumer: (context: React.Context<any>, contextName: string) => (Component: React.ComponentType) => (props: object) => JSX.Element;
export declare const composeConsumers: (...consumers: any[]) => React.Context<any>;
