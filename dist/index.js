"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.composeConsumers = exports.createConsumer = void 0;
const fp_1 = require("lodash/fp");
const React = require("react");
exports.createConsumer = (context, contextName) => (Component) => (props) => {
    return React.createElement(context.Consumer, null, value => React.createElement(Component, Object.assign({}, Object.assign({ [contextName]: value }, props))));
};
exports.composeConsumers = (...consumers) => {
    return fp_1.compose(...consumers);
};
//# sourceMappingURL=index.js.map