import { compose } from 'lodash/fp'
import * as React from 'react';

export const createConsumer = (context: React.Context<any>, contextName: string) => (Component: React.ComponentType) => (props: object) => {
    return <context.Consumer>{value => <Component {...{ [contextName]: value, ...props }} />}</context.Consumer>
}

export const composeConsumers = (...consumers): React.Context<any> => {
    return compose(...consumers) as unknown as React.Context<any>
}
